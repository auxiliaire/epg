EPG Case Study by Viktor Daróczi
================================

This is the 'frontend' part of my solution to the assignment.

There is also a 'backend' part, which is a RESTful API, featuring one endpoint.

What's inside?
--------------

Features:

  * Symfony 3

  * OOP PHP

  * A working Http client based on Curl

  * Checkout form
  
  * A working API library
  
  * Authorization with Bearer token
  
  * Https
  
  * Basic error handling

  * PHPUnit tests

Additional thoughts
-------------------

In general, I consider this a tech preview or a sketch. It leaves a lot 
of features unimplemented. I concentrated on creating a working end-to-end
solution.

Authorization
-------------

I used only a base64 encrypted string, but in a real world application
I would use OAuth or a preshared, non-guessable (non linear) secret key
which would be granted to a specific client (front), and could be easily
revoked.

Database storage
----------------

Credit card data is a sensitive set of information. I would not store a
card number without proper encryption. I think the encryption must use
a private key for encryption, and must use a kind of salting. I would use
Onion encryption with Blowfish algorithm. I would store the credit card 
numbers in two or three different fields:

  * The last four digits of the number for human identification
  
  * The encrypted number

  * (A salted hash for comparison)
  
The same applies for Cvv number and expiration dates, which together form
a sensitive set of data. In case of such encryption, even a database breach
wouldn't expose the sensitive information.

In a real world database, I would have one table for the client, one table
for card data. These are 1-n connected. Typically a user would preexists 
the payment. Existing users could choose from previously stored cards by
the last four digits. If it is found in the database, then the app should
use that instead of requesting input. If not, then a new checkout would
create a new record in the card table. The amount and currency again should
be stored elsewhere, because it is a transaction or payment history (may
be both). The relation between card and checkout would be 1-n as well.
A transaction table would feature init date, finish date exact timestamps,
a status (success, canceled, failed, etc) and flags like active, in-progress,
etc.

Logging
-------

As for the logging, I consider crucial not to store any sensitive information.
The log tables should leave sensitive information blank. These are typically
input values from the client, which may or may not be validated, yet could
contain valid and exploitable data.

Instead of storing all data which is sent by post or API requests, I would
filter sensitive data out and store only transaction variables like:
  
  * Headers sent
  
  * Response headers
  
  * Exact timestamps
  
  * Insensitive data for tx identification