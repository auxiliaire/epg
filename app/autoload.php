<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';

$mapping = require __DIR__.'/../src/library/mock-api-client/class_map.php';
$loader->addClassMap($mapping);

AnnotationRegistry::registerLoader([$loader, 'loadClass']);

return $loader;
