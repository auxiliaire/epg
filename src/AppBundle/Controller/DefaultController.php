<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Checkout;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\CheckoutType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $checkout = $this->getCheckout();

        $form = $this->createForm(CheckoutType::class, $checkout);

        $form->handleRequest($request);

        $apiCheckout = null;
        $env = $this->get('kernel')->getEnvironment();
        $isProd = ($env == 'prod');
        if ($form->isValid()) {
            $api = \Mock\Api::getInstance(!$isProd, $this->container->getParameter('epgapi_secret'));
            /* @var $apiCheckout \Mock\Api\Checkout */
            $apiCheckout = $api->checkout($checkout->__toArray())->save();
        }

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'form' => $form->createView(),
            'price' => $checkout->getAmount(),
            'currency' => $checkout->getCurrency(),
            'checkout' => $apiCheckout
        ]);
    }

    /**
     * Returns a prefilled Checkout instance.
     * On a real scenario this would typically come from a service with a certain business logic.
     * @return Checkout
     */
    private function getCheckout() {
        $checkout = new Checkout();
        $checkout->setAmount($this->getPaymentAmount());
        $checkout->setCurrency($this->getPaymentCurrency());
        return $checkout;
    }

    /**
     * The amount and currency should come from a service which decides the proper amount to apply
     * according to a predetermined business logic.
     * For the sake of brevity, I only use dummy hardcoded values here.
     * @return float
     */
    private function getPaymentAmount() {
        return 12.00; // Use a value > 100 to get 'exceed' API response.
    }

    /**
     * @return string
     */
    private function getPaymentCurrency() {
        return 'EUR';
    }
}
