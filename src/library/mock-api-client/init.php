<?php

use Symfony\Component\ClassLoader\ClassMapGenerator;

ClassMapGenerator::dump(__DIR__ . '/lib', __DIR__ . '/class_map.php');