<?php

namespace Mock;

use Mock\Api\Checkout;

class Api
{
    const CERT_FILE = "/../data/ca-certificate.crt";

    private $testMode = null;
    private static $apiKey = null;
    private $certificate = null;
    private static $instance = null;

    public function __construct($testMode)
    {
        // Test mode may be determined from apiKey as well, but for the sake of simplicity I have only one apiKey.
        $this->testMode = $testMode;
        $this->certificate = realpath(__DIR__ . self::CERT_FILE);
    }

    public function checkout($params)
    {
        return new Checkout($params);
    }

    public static function getInstance($testMode = false, $apiKey = null)
    {
        if (null !== $apiKey) {
            self::$apiKey = $apiKey;
        }
        if (null === self::$instance) {
            self::$instance = new Api($testMode, $apiKey);
        }
        return self::$instance;
    }

    public function isTestMode()
    {
        return (bool)$this->testMode;
    }

    public function getCertificate()
    {
        return $this->certificate;
    }

    public static function setApiKey($apiKey)
    {
        self::$apiKey = $apiKey;
    }

    public static function getApiKey()
    {
        return self::$apiKey;
    }
}