<?php

namespace Mock\Api;

/**
 * Class Checkout
 * @package Mock\Api
 * @method string getResult()
 * @method int getResultCode()
 * @method int getId()
 * @method string getResultMessage()
 */
class Checkout extends Resource
{
    protected static $path = '/checkout';


}