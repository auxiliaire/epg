<?php

namespace Mock\Api;

interface ISerializable {

    public function serialize();
}