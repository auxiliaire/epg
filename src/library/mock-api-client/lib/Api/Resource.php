<?php

namespace Mock\Api;


use Mock\Api;
use Mock\HttpClient\CurlClient;

class Resource implements ISerializable
{
    const API_URL = 'https://api-epg.localproject.hu';

    protected static $path = '/';
    protected $values = null;
    protected static $httpClient = null;
    /**
     * @var Response
     */
    protected $response = null;

    public function __construct($params)
    {
        $this->values = array();
        if (is_array($params)) {
            foreach ($params as $k => $v) {
                if (is_string($k)) {
                    $this->values[$k] = $v;
                }
            }
        }
    }

    /**
     * @return Resource
     * @throws \Mock\HttpClient\HttpClientException
     */
    public function save()
    {
        $url = self::API_URL . $this->getPath();
        $params = $this->__toJson();
        $headers = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($params),
            'Authorization: Bearer ' . Api::getApiKey()
        );
        $this->response = new Response($this->getHttpClient()->request('post', $url, $headers, $params));

        if ($this->response->getHttpCode() == 200) {
            $body = $this->response->getBody();
            $data = json_decode($body);
            if (false !== $data) {
                $this->values = array();
                foreach ($data as $k => $v) {
                    if (is_string($k)) {
                        $this->values[$k] = $v;
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    protected function getHttpClient()
    {
        if (null === self::$httpClient) {
            $api = Api::getInstance();
            self::$httpClient = new CurlClient($api->isTestMode(), $api->getCertificate());
        }
        return self::$httpClient;
    }

    public function getPath()
    {
        return static::$path;
    }

    public function __set($k, $v)
    {
        $this->values[$k] = $v;
    }

    public function &__get($k)
    {
        $value = null;
        if (array_key_exists($k, $this->values)) {
            $value = $this->values[$k];
        } else {
            $class = get_class($this);
            error_log("Mock Notice: Undefined property $k in class $class");
        }
        return $value;
    }

    public function __call($name, $arguments)
    {
        if (0 === strpos($name, 'get')) {
            $k = lcfirst(substr($name, 3));
            if (array_key_exists($k, $this->values)) {
                return $this->values[$k];
            }
        }
        // TODO: consider throwing an exception instead
        return null;
    }

    public function __isset($name)
    {
        return isset($this->values[$name]);
    }

    public function serialize()
    {
        return $this->__toArray();
    }

    public function __toArray()
    {
        return $this->values;
    }

    public function __toJson()
    {
        return json_encode($this->__toArray());
    }
}