<?php
/**
 * Created by PhpStorm.
 * User: vezir
 * Date: 2/24/16
 * Time: 11:20 AM
 */

namespace Mock\Api;


class Response
{
    /**
     * @var int
     */
    private $httpCode = null;
    /**
     * @var string
     */
    private $body = null;
    /**
     * @var array
     */
    private $headers = null;
    /**
     * @var string
     */
    private $error = null;
    /**
     * @var array
     */
    private $info = null;

    public function __construct($curlResponse = null)
    {
        if (is_array($curlResponse)) {
            list($this->httpCode, $this->body, $this->headers, $this->error, $this->info) = $curlResponse;
        }
    }

    /**
     * Dummy validator function.
     * In production this would do sophisticated checks to ensure that it is a proper response.
     * Business logic would decide what is a proper response.
     *
     * This function is not used anyways.
     * @return bool
     */
    public function isValid()
    {
        return (
            ($this->getBody() !== false)
            && ($this->getError() !== null)
            && ($this->getError() === '')
        );
    }

    /**
     * @return int
     */
    public function getHttpCode()
    {
        return $this->httpCode;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return $this->info;
    }

}