<?php

namespace Mock\HttpClient;


class CurlClient implements IClient
{
    const DEFAULT_TIMEOUT = 80;
    const DEFAULT_CONNECT_TIMEOUT = 30;

    private $testMode = null;
    private $certificate = null;
    private $responseHeaders = null;

    public function __construct($testMode = false, $cert = null)
    {
        $this->testMode = $testMode;
        $this->certificate = $cert;
    }

    public function request($method, $url, $headers, $params) {
        $ch = curl_init();
        $this->responseHeaders = array();

        $opts = array();

        $opts[CURLOPT_URL] = $url;

        switch (strtolower($method)) {
            case 'get':
                $opts[CURLOPT_HTTPGET] = 1;
                // TODO: implement params
                break;
            case 'post':
                $opts[CURLOPT_POST] = 1;
                $opts[CURLOPT_POSTFIELDS] = $params;
                break;
            default:
                throw new HttpClientException("Unimplemented method ($method).");
        }

        $opts[CURLOPT_RETURNTRANSFER] = true;
        $headers = array_merge($headers, array(
            // TODO: fix headers come here
        ));
        $opts[CURLOPT_CONNECTTIMEOUT] = self::DEFAULT_CONNECT_TIMEOUT;
        $opts[CURLOPT_TIMEOUT] = self::DEFAULT_TIMEOUT;
        $opts[CURLOPT_HTTPHEADER] = $headers;
        $opts[CURLOPT_SSL_VERIFYPEER] = !$this->testMode;
        $opts[CURLOPT_SSL_VERIFYHOST] = $this->testMode? 0: 2;
        if ($this->certificate) {
            $opts[CURLOPT_CAINFO] = $this->certificate;
        }
        $opts[CURLOPT_HEADERFUNCTION] = array(&$this, 'curlHeaderCallback');

        curl_setopt_array($ch, $opts);

        $response = curl_exec($ch);
        if($response === false) {
            $error = curl_error($ch);
        } else {
            $error = '-';
        }
        $info = curl_getinfo($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        return array($httpCode, $response, $this->responseHeaders, $error, $info);
    }

    public function curlHeaderCallback($curl, $line) {
        if (strpos($line, ":") !== false) {
            list($key, $value) = explode(":", trim($line), 2);
            $this->responseHeaders[trim($key)] = trim($value);
        }
        return strlen($line);
    }
}