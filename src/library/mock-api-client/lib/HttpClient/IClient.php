<?php

namespace Mock\HttpClient;

interface IClient {
    public function request($method, $url, $headers, $params);
}