<?php

namespace Tests\Mock\Resource;

use Mock\Api\Resource;

class ResourceTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructorDefaults()
    {
        $r = new Resource(null);
        $this->assertInternalType('array', $r->__toArray());
        $this->assertEmpty($r->__toArray());
    }

    public function testToJson()
    {
        $r = new Resource(null);
        $this->assertEquals('[]', $r->__toJson());
    }

    public function testConstructorParams()
    {
        $a = 'a';
        $b = 2.00;
        $c = array('a' => $a, 'b' => $b);
        $r = new Resource(array(
            'a' => $a,
            'b' => $b,
            'c' => $c
        ));
        $this->assertEquals($a, $r->a);
        $this->assertEquals($b, $r->b);
        $this->assertEquals($c, $r->c);
    }

    public function testGettersAndSetters()
    {
        $r = new Resource(null);
        $a = 'a';
        $b = 2.00;
        $c = array('a' => $a, 'b' => $b);
        $r->a = $a;
        $r->b = $b;
        $r->c = $c;
        $this->assertEquals($a, $r->a);
        $this->assertEquals($b, $r->b);
        $this->assertEquals($c, $r->c);
    }

    public function testCall()
    {
        $r = new Resource(null);
        $this->assertNull($r->getNonExistant());
        $a = 'a';
        $b = 2.00;
        $c = array('a' => $a, 'b' => $b);
        $r->a = $a;
        $r->b = $b;
        $r->c = $c;
        $this->assertEquals($a, $r->getA());
        $this->assertEquals($b, $r->getB());
        $this->assertEquals($c, $r->getC());
    }
}