<?php

namespace Tests\Mock\HttpClient;

use Mock\Api\Response;

class ResponseTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructorDefaults()
    {
        $r = new Response();
        $this->assertNull($r->getBody());
        $this->assertNull($r->getError());
        $this->assertNull($r->getHeaders());
        $this->assertNull($r->getHttpCode());
        $this->assertNull($r->getInfo());
        $this->assertFalse($r->isValid());
    }

    public function testConstructorParams()
    {
        $httpCode = 200;
        $body = 'body';
        $headers = array();
        $error = 'error';
        $info = array();
        $r = new Response(array(
            $httpCode,
            $body,
            $headers,
            $error,
            $info
        ));
        $this->assertEquals($httpCode, $r->getHttpCode());
        $this->assertEquals($body, $r->getBody());
        $this->assertEquals($headers, $r->getHeaders());
        $this->assertEquals($error, $r->getError());
        $this->assertEquals($info, $r->getInfo());
    }
}