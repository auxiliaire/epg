<?php

namespace Tests\Mock\HttpClient;

use Mock\HttpClient\CurlClient;

class CurlClientTest extends \PHPUnit_Framework_TestCase
{
    public function testRequest()
    {
        $client = new CurlClient();
        $response = $client->request('get', 'http://www.google.com', array(), array());
        list($httpCode, $body, $headers, $error, $info) = $response;
        $this->assertInternalType('array', $response);
        $this->assertInternalType('int', $httpCode);
        $this->assertContains('google', $body);
        $this->assertInternalType('array', $headers);
        $this->assertInternalType('string', $error);
        $this->assertInternalType('array', $info);
    }

    // Should test thoroughly: get, post, exceptions, redirect, various response codes, headers, etc.
}