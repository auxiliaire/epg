<?php

namespace Tests\Mock\HttpClient;

use Mock\HttpClient\HttpClientException;

class HttpClientExceptionTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructorDefaults()
    {
        $e = new HttpClientException();
        $this->assertEquals('', $e->getMessage());
        $this->assertEquals(0, $e->getCode());
        $this->assertNull($e->getPrevious());
    }

    public function testMessage()
    {
        $e = new HttpClientException('msg');
        $this->assertEquals('msg', $e->getMessage());
    }

    public function testCode()
    {
        $e = new HttpClientException('msg', 100);
        $this->assertEquals(100, $e->getCode());
    }

    public function testPrevious()
    {
        $p = new HttpClientException('p', 0);
        $e = new HttpClientException('e', 0, $p);
        $this->assertEquals($p, $e->getPrevious());
    }

    public function testToString()
    {
        $p = new HttpClientException('p', 0);
        $e = new HttpClientException('e', 0, $p);
        $s = $e->__toString();
        $this->assertContains('p', $s);
        $this->assertContains('Next', $s);
        $this->assertContains('e', $s);
    }

}